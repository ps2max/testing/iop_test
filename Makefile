include $(PS2SDKSRC)/Defs.make

IOP_BIN = test.irx
IOP_OBJS = main.o imports.o
IOP_INCS := -I$(PS2SDKSRC)/iop/kernel/include -I$(PS2SDKSRC)/common/include
IOP_CFLAGS := -D_IOP -fno-builtin -O0 -G0 -Wall -Werror $(IOP_INCS)
IOP_LDFLAGS := -nostdlib -s

all: $(IOP_BIN)

dunp:
	$(IOP_TOOL_PREFIX)objdump -x $(IOP_BIN) > dump.txt
	$(IOP_TOOL_PREFIX)objdump -d $(IOP_BIN) > dis.txt

clean:
	rm -rf $(IOP_BIN) $(IOP_OBJS) imports.c

run:
	ps2client -h 192.168.1.10 execiop host:$(IOP_BIN)

%.o: %.c
	$(IOP_CC) $(IOP_CFLAGS) -c $< -o $@
	$(IOP_CC) $(IOP_CFLAGS) -S $< -o $@.S

imports.c: imports.lst
	$(ECHO) "#include \"irx_imports.h\"" > $@
	cat $< >> $@

$(IOP_BIN): $(IOP_OBJS)
	$(IOP_CC) $(IOP_CFLAGS) -o $(IOP_BIN) $(IOP_OBJS) $(IOP_LDFLAGS)
