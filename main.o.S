	.file	1 "main.c"
	.section .mdebug.abi32
	.previous
	.nan	legacy
	.module	fp=32
	.module	nooddspreg
	.text
	.globl	_irx_id
	.rdata
	.align	2
$LC0:
	.ascii	"test\000"
	.data
	.align	2
	.type	_irx_id, @object
	.size	_irx_id, 8
_irx_id:
	.word	$LC0
	.half	257
	.space	2
	.rdata
	.align	2
$LC1:
	.ascii	"Hello World!\012\000"
	.text
	.align	2
	.globl	_start
	.set	nomips16
	.set	nomicromips
	.ent	_start
	.type	_start, @function
_start:
	.frame	$fp,24,$31		# vars= 0, regs= 2/0, args= 16, gp= 0
	.mask	0xc0000000,-4
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	addiu	$sp,$sp,-24
	sw	$31,20($sp)
	sw	$fp,16($sp)
	move	$fp,$sp
	sw	$4,24($fp)
	sw	$5,28($fp)
	lui	$2,%hi($LC1)
	addiu	$4,$2,%lo($LC1)
	jal	printf
	nop

	li	$2,1			# 0x1
	move	$sp,$fp
	lw	$31,20($sp)
	lw	$fp,16($sp)
	addiu	$sp,$sp,24
	jr	$31
	nop

	.set	macro
	.set	reorder
	.end	_start
	.size	_start, .-_start
	.ident	"GCC: (GNU) 9.2.0"
