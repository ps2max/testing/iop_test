/*
 * irx_imports.h - Defines all IRX imports.
 *
 * Copyright (c) 2009 PS2DEV.org
 *
 * See the file LICENSE included with this distribution for licensing terms.
 */

#ifndef IOP_IRX_IMPORTS_H
#define IOP_IRX_IMPORTS_H

#include "irx.h"

/* Please keep these in alphabetical order!  */
#include "stdio.h"

#endif /* IOP_IRX_IMPORTS_H */

